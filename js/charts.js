var lineChartContext = document.getElementById('lineChart');

var lineChartData = {
    labels: ['10', '11', '12', '13', '14'],
    datasets: [
        {
            label: 'Active containers by hours',
            data: [5, 4, 5, 6, 7],
            borderColor: "rgba(75,192,192,1)",
            backgroundColor: "rgba(75,192,192,0.4)",
        }
    ] 
};

var lineChart = new Chart(lineChartContext, {
    type: 'line',
    data: lineChartData
});

var barChartContext = document.getElementById('barChart');

var barChartData = {
    labels: ['10', '11', '12', '13', '14'],
    datasets: [
        {
            label: 'Active nodes by hours',
            data: [1, 2, 2, 2, 4],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
        }
    ] 
};

var barChart = new Chart(barChartContext, {
    type: 'bar',
    data: barChartData
});
